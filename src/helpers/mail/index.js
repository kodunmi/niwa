const {transporter, MailGenerator} = require("./mail");


function sendMail(response, messageto, subjectMessage){
    console.log(response)

    let mail = MailGenerator.generate(response);

    let message = {
        from: "noreply@beeki.com",
        to: messageto,
        subject: subjectMessage,
        html: mail,
    };

    transporter
    .sendMail(message)
    .then(() => {
        console.log("this message was sent to", messageto)
    })
    .catch((error) => console.error(error));
}

// module.exports.sendMail() = async(parent, args, context, info) => {

//     console.log("parents: " + parent);
    
// }


module.exports = {sendMail}