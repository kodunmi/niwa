const { verify } = require('jsonwebtoken')
const cloudinary = require('./helpers/cloudinary')
const { GraphQLUpload } = require('graphql-upload');
const APP_SECRET = 'appsecret321'
const { asNexusMethod } = require('nexus')
const Upload = asNexusMethod(GraphQLUpload, 'upload')

function getUserId(context) {
  const Authorization = context.request.get('Authorization')
  if (Authorization) {
    const token = Authorization.replace('Bearer ', '')
    const verifiedToken = verify(token, APP_SECRET)
    return verifiedToken && verifiedToken.userId
  }
}

const uploadFile = async (file) => {
  return new Promise((resolve, reject) => {
    cloudinary.uploader.upload(file,{invalidate:true},function (
      error,
      result
    ) {
      if (result) {
        resolve(result)
      } else {
        reject(error);
      }
    });
  })
};

const deleteFile = (public_id) => {
  return new Promise((resolve, reject) => {
    cloudinary.uploader.destroy(public_id, function (error, result) {
      if (result) {
        resolve(result)
      } else {
        reject(error);
      }
    })
  })
}

const updateFile = async (file, public_id) => {
  return new Promise((resolve, reject) => {
    cloudinary.uploader.upload(file,{public_id:public_id, invalidate:true},function (
      error,
      result
    ) {
      if (result) {
        resolve(result)
      } else {
        reject(error);
      }
    });
  })
}

module.exports = {
  getUserId,
  uploadFile,
  updateFile,
  deleteFile,
  APP_SECRET,
  Upload,
}
