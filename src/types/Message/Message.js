const { objectType } = require('nexus')

const Message = objectType({
  name: 'Message',
  definition(t) {
    t.model.id()
    t.model.lastname()
    t.model.firstname()
    t.model.email()
    t.model.phone()
    t.model.subject()
    t.model.message()
    t.model.createdAt()
    t.model.updatedAt() 
  },
})

module.exports = {
  Message,
}
