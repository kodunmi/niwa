const { nullable, stringArg, nonNull, extendType, intArg, arg } = require("nexus")

const MessageMutation = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createMessage', {
            type: 'Message',
            args: {
                firstname: nonNull(stringArg()),
                lastname: nonNull(stringArg()),
                phone: nonNull(stringArg()),
                email: nullable(stringArg()),
                subject: nonNull(stringArg()),
                message: nonNull(stringArg()),
            },
            resolve: async (p, {firstname, lastname, phone, email, subject, message }, ctx) => {

                return await ctx.prisma.message.create({
                    data: {
                        firstname,
                        lastname,
                        email,
                        phone,
                        subject,
                        message
                    }
                })
            }
        })

        t.field('deleteMessage', {
            type: 'Message',
            args: {
                id : nonNull(intArg())
            },
            resolve: async (p, { id }, ctx) => {
                return await ctx.prisma.message.delete({
                    where: {
                        id
                    }
                })
            }
        })

        t.field('deleteAllMessage', {
            type: 'Message',
            
            resolve: async (p, { }, ctx) => {
                return await ctx.prisma.message.deleteMany({ })
            }
        })

    }
})

module.exports = {
    MessageMutation
}