module.exports ={
    ...require('./Mutation'),
    ...require('./Message'),
    ...require('./Query')
}