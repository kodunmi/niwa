module.exports = {
  ...require('./Administrator'),
  ...require('./Blog'),
  ...require('./Department'),
  ...require('./Settings'),
  ...require('./Management'),
  ...require('./Unit'),
  ...require('./Vessel'),
}