const { objectType } = require('nexus')

const Setting = objectType({
  name: 'Setting',
  definition(t) {
    t.model.id()
    t.model.address()
    t.model.phone()
    t.model.email()
    t.model.facebook()
    t.model.telegram()
    t.model.instagram()
    t.model.twitter()
    t.model.youtube()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Setting,
}
