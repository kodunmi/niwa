const { queryType, extendType } = require('nexus')

const SettingQuery = extendType({
  type:'Query',
  definition(t) {
    t.field('setting', {
      type: 'Setting',
      
      resolve: (parent, ctx) => {
        return ctx.prisma.setting.findFirst({})
      },
    })
  },
})

module.exports = {
  SettingQuery,
}
