module.exports = {
    ...require('./Setting'),
    ...require('./Mutations'),
    ...require('./Query'),
}