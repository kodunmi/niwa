const { nullable, stringArg, extendType, arg } = require("nexus")
const { uploadFile, Upload, deleteFile, updateFile } = require('../../utils')

const SettingMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('addSetting',{
      type: 'Setting',
      args: {
        address: nullable(stringArg()),
        email: nullable(stringArg()),
        phone: nullable(stringArg()),
        facebook: nullable(stringArg()),
        telegram: nullable(stringArg()),
        instagram: nullable(stringArg()),
        twitter: nullable(stringArg()),
        youtube: nullable(stringArg()),
        logo: nullable(arg({type: Upload}))
      },

      resolve: async (p,{address, email, phone, logo, facebook, telegram, instagram, twitter, youtube}, ctx) => {
        const { secure_url, public_id } = logo ? await uploadFile(logo) : {}

        return await ctx.prisma.setting.create({
          data: {
            logo: secure_url,
            public_id,
            address,
            phone,
            email,
            telegram,
            facebook,
            instagram,
            twitter,
            youtube
          }
        })
      }
    })


    t.field('editSetting',{
      type: 'Setting',
      args: {
        address: nullable(stringArg()),
        email: nullable(stringArg()),
        phone: nullable(stringArg()),
        facebook: nullable(stringArg()),
        telegram: nullable(stringArg()),
        instagram: nullable(stringArg()),
        twitter: nullable(stringArg()),
        youtube: nullable(stringArg()),
        logo: nullable(arg({type: Upload}))
      },

      resolve: async (p,{id, address, email, phone, logo, facebook, telegram, instagram, twitter, youtube}, ctx) => {

        const setting = await ctx.prisma.setting.findFirst({})

        if (image) file = await updateFile(logo, setting.public_id)
        
        return await ctx.prisma.setting.update({
          where:{
            id: setting.id
          },
          data: {
            logo: image ? file.secure_url : undefined,
            public_id: image ? file.public_id : undefined,
            address,
            phone,
            email,
            telegram,
            facebook,
            instagram,
            twitter,
            youtube
          }
        })
      }
    })
  }
})
module.exports = {
  SettingMutation,
}