const { queryType, extendType } = require('nexus')

const OperatorQuery = extendType({
  type:'Query',
  definition(t) {
   t.crud.operators({
     ordering: true,
     filtering: true
   })
  },
})

module.exports = {
  OperatorQuery,
}
