const { objectType } = require('nexus')

const Operator = objectType({
  name: 'Operator',
  definition(t) {
    t.model.id()
    t.model.lastname()
    t.model.firstname()
    t.model.email()
    t.model.phone()
    t.model.verified()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Operator,
}
