const { RuleTrue } = require("graphql-shield/dist/rules")
const { nullable, stringArg, nonNull, extendType, intArg , arg} = require("nexus")

const OperatorMutation = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createOperator',{
            type: 'Operator',
            args:{
                firstname: nonNull(stringArg()),
                lastname: nonNull(stringArg()),
                email: nonNull(stringArg()),
                phone: nonNull(stringArg())
            },
            resolve: async (p, {firstname, lastname, email, phone}, ctx) =>{
                return await ctx.prisma.operation.create({
                    data:{
                        firstname,
                        lastname,
                        email,
                        phone
                    }
                })
            }
        })

        t.field('deleteOperator', {
            type: 'Operator',
            args: {
                email: nonNull(stringArg())
            },
            resolve: async (p,{email}, ctx) =>{
                return await ctx.prisma.operation.delete({
                    where: {
                        email
                    }
                })
            }
        })

    }
})

module.exports = { 
    OperatorMutation
}