module.exports ={
    ...require('./Mutation'),
    ...require('./Operator'),
    ...require('./Query')
}