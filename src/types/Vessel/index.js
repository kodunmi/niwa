module.exports = {
    ...require('./Mutations'),
    ...require('./Query'),
    ...require('./Vessel')
}