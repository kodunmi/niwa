const { nullable, nonNull, stringArg, extendType, intArg } = require("nexus")
const cryptoRandomString = require('crypto-random-string');

const VesselMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('createVessel', {
      type: 'Vessel',
      args: {
        name: nonNull(stringArg()),
        owner: nonNull(stringArg()),
        type: nonNull(stringArg())
      },
      resolve: async (p, { name, owner, type }, ctx) => {

        const code = cryptoRandomString({ length: 15, type: 'numeric' })

        return await ctx.prisma.vessel.create({
          data: {
            name,
            owner,
            type,
            code
          }
        })
      }
    })

    t.field('editVessel', {
      type: 'Vessel',
      args: {
        id: nonNull(stringArg()),
        name: nullable(stringArg()),
        owner: nullable(stringArg()),
        type: nullable(stringArg())
      },
      resolve: async (p, { id, name, owner, type }, ctx) => {
        return await ctx.prisma.vessel.update({
          where: {
            id: id
          },
          data: {
            name,
            owner,
            type
          }
        })
      }
    })

    t.field('deleteVessel', {
      type: 'Vessel',
      args: {
        id: nonNull(stringArg()),
      },
      resolve: async (p, { id }, ctx) => {
        return await ctx.prisma.vessel.delete({
          where: {
            id: id
          }
        })
      }
    })

    t.field('verifyVessel',{
      type: 'Vessel',
      args:{
        code: nonNull(intArg())
      },

      resolve: async (p, {code}, ctx) =>{
        const vessel = await ctx.prisma.vessel.findUnique({
          where:{
            code: code
          }
        })

        if(!vessel) return new Error("no vessel with the code")

        return vessel
      }
    })
  }
})
module.exports = {
  VesselMutation,
}