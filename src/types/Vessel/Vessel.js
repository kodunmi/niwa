const { objectType } = require('nexus')

const Vessel = objectType({
  name: 'Vessel',
  definition(t) {
    t.model.id()
    t.model.name()
    t.model.owner()
    t.model.code()
    t.model.type()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Vessel,
}
