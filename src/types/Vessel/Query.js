const {queryType, extendType} = require('nexus')

const VesselQuery = extendType({
  type:'Query',
  definition(t) {
    t.crud.vessels({
      filtering: true,
      ordering: true
    })
  },
})

module.exports = {
  VesselQuery,
}
