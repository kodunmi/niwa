const { extendType } = require('nexus')

const AdminQuery = extendType({
  type: 'Query',
  definition(t) {
    t.crud.admins({
        filtering: true,
        ordering: true,
      });
  },
})

module.exports = {
  AdminQuery,
}
