const { objectType } = require('nexus')

const Admin = objectType({
  name: 'Admin',
  definition(t) {
    t.model.id()
    t.model.email()
    t.model.password()
    t.model.profile()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Admin,
}
