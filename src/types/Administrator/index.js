module.exports = {
    ...require('./Admin'),
    ...require('./AuthPayload'),
    ...require('./Mutations'),
    ...require('./Profile'),
    ...require('./Query'),
}