const { compare, hash } = require('bcryptjs')
const { sign } = require('jsonwebtoken')
const { nullable, stringArg, arg , intArg, nonNull, extendType} = require('nexus')
const { APP_SECRET, uploadFile, Upload, deleteFile, updateFile } = require('../../utils')
const sendMail = require("../../helpers/mail/index");

const AdminMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('signup', {
      type: 'AuthPayload',
      args: {
        firstname: nonNull(stringArg()),
        lastname: nonNull(stringArg()),
        email: nonNull(stringArg()),
        password: nonNull( stringArg()),
        phone: nullable(stringArg()),
        image: nullable(arg({type: Upload})),
        accountType: nullable(stringArg())
      },
      resolve: async (parent, {image, firstname, lastname, email, password, phone, accountType }, ctx) => {
        const hashedPassword = await hash(password, 10)
    
        const admincheck = await ctx.prisma.admin.findUnique({
          where: {
            email: email
          }
        })

        if(admincheck) return new Error("email already taken") 

        const { secure_url, public_id } = image ? await uploadFile(image) : {}

        const admin = await ctx.prisma.admin.create({
          data: {
            email,
            password: hashedPassword,
            accountType, 
            profile: {
              create: {
                phone,
                firstname,
                lastname,
                image: image ? secure_url : undefined,
                public_id: image ?  public_id : undefined,
              }
            }
          },
        })

        //send verification mail
        let response = {
            body: {
              name: firstname,
              intro: 'your admin account has been created',
              action: {
                instructions: 'to continue, please click here:',
                button: {
                  color: '#22BC66', // Optional action button color
                  text: 'login to your account',
                  link: 'https://mailgen.js/confirm?s=d9729feb74992cc3482b350163a1a010'
                },
                outro: 'If you did not create an account, no further action is required. /n Thanks'
              },
            }
          }
  
          sendMail.sendMail(response, admin.email, "account created");

        return {
          token: sign({ userId: admin.id }, APP_SECRET),
          admin,
        }
      },
    })

    t.field('login', {
      type: 'AuthPayload',
      args: {
        email: nonNull(stringArg()) ,
        password: nonNull(stringArg()),
      },
      resolve: async (parent, { email, password }, context) => {
        const admin = await context.prisma.admin.findUnique({
          where: {
            email,
          },
        })
        if (!admin) {
          throw new Error(`no admin found for email: ${email}`)
        }
        const passwordValid = await compare(password, admin.password)
        if (!passwordValid) {
          throw new Error('invalid password')
        }
        return {
          token: sign({ userId: admin.id }, APP_SECRET),
          admin,
        }
      },
    })


    t.field('sendPasswordResetMail', {
      type: 'Admin',
      args: {
        email: nonNull(stringArg())
      },
      resolve: async (p, { email }, context) => {
        const admin = await context.prisma.admin.findUnique({
          where: {
            email: email
          },
          select: {
            profile: {

            }
          }
        })

        if (!admin) return new Error('no admin with such email')

        let response = {
          body: {
            name: admin.profile.firstname,
            intro: `you requested for a password reset`,
            action: {
              instructions: 'to reset password, please click here:',
              button: {
                color: '#22BC66', // Optional action button color
                text: 'reset password',
                link: `https://biki.com/password-reset?c=${admin.password}?d=${Date.now}`
              },
              outro: 'If you did not request for password reset, no further action is required. /n Thanks'
            },
          }
        }

        sendMail.sendMail(response, admin.email, "reset password");

        return admin

      }
    })

    t.field('resetPassword', {
      type: 'Admin',
      args: {
        id: nonNull(intArg()),
        password: nonNull(stringArg({description: "the new password to be stored in the database" }))
      },

      resolve: async (p, { id, password }, context) => {

        const checkadmin = await context.prisma.admin.findUnique({
          where: {
            id: id
          },

          select: {
            profile: {

            }
          }

        })

        if (!checkuser) return new Error('no such admin is found')

        const hashedpassword = await hash(password, 10)
        const admin = await context.prisma.admin.update({
          where: {
            id: id
          },
          data: {
            password: hashedpassword
          }
        })

        let response = {
          body: {
            name: checkadmin.profile.firstname,
            intro: `your password has been reset`,
            action: {
              instructions: 'to login, please click here:',
              button: {
                color: '#22BC66', // Optional action button color
                text: 'login to dashboard',
                link: `https://biki.com/login`
              },
              outro: 'If you did not change your password, please contact admin@biki.com for clearfication. /n Thanks'
            },
          }
        }

        sendMail.sendMail(response, admin.email, "password updated successfully");

        return admin
      }
    })

    t.field('profileUpdate', {
      type: 'Admin',
      args: {
        id: nonNull(intArg()),
        firstname: stringArg(),
        lastname: stringArg(),
        phone: stringArg(),
        email: stringArg(),
        password: stringArg(),
        image: arg({
          type: Upload,
        }),
      },
      resolve: async (parent, { id, firstname, lastname, phone, image, email, password }, ctx) => {
        const hashedPassword = password ? await hash(password, 10) : undefined

        const admincheck = await ctx.prisma.admin.findUnique({
          where:{
            id:id
          },
          select:{
            profile:{
              select:{
                public_id:true
              }
            }
          }
        })

        if(!admincheck) return new Error("no admin found with the id")

        if(image) file = await updateFile(image, admincheck.profile.public_id)

        return await ctx.prisma.admin.update({
          where: {
            id: id
          },
          data: {
            email,
            password: hashedPassword,
            profile: {
              update: {
                firstname,
                lastname,
                phone,
                secure_url: file.secure_url,
                public_id: file.public_id
              }
            }
          },
        })
      },
    })
  },
})

module.exports = {
  AdminMutation,
}
