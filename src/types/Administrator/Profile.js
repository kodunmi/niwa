const { objectType } = require('nexus')

const Profile = objectType({
  name: 'Profile',
  definition(t) {
    t.model.id()
    t.model.admin()
    t.model.firstname()
    t.model.lastname()
    t.model.phone()
    t.model.secure_url()
    t.model.public_id()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Profile,
}
