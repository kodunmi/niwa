const { objectType } = require('nexus')

const AuthPayload = objectType({
  name: 'AuthPayload',
  definition(t) {
    t.string('token')
    t.field('admin', { type: 'Admin' })
  },
})

module.exports = { AuthPayload }
