const { nullable, nonNull, stringArg, extendType } = require("nexus")

const UnitMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('createUnit',{
      type: 'Unit',
      args: {
        name: nonNull(stringArg()),
        department: nonNull(stringArg())
      },
      resolve: async (p, {name, department}, ctx) => {
        return await ctx.prisma.unit.create({
          data: {
            name,
            department: {
              connect :{
                id: department
              }
            }
          }
        })
      }
    })

    t.field('editUnit',{
      type: 'Unit',
      args: {
        id: nonNull(stringArg()),
        name: nullable(stringArg()),
        department: nullable(stringArg())
      },
      resolve: async (p, {id , name, department}, ctx) => {
        return await ctx.prisma.unit.update({
          where:{
            id:id
          },
          data: {
            name,
            department: {
              connect :{
                id: department
              }
            }
          }
        })
      }
    })
    t.field('deleteUnit',{
      type: 'Unit',
      args: {
        id: nonNull(stringArg()),
      },
      resolve: async (p, {id , name, department}, ctx) => {
        return await ctx.prisma.unit.delete({
          where:{
            id:id
          }
        })
      }
    })
  }
})
module.exports = {
  UnitMutation,
}