const {queryType, extendType} = require('nexus')

const UnitQuery = extendType({
  type:'Query',
  definition(t) {
    t.crud.units({
      filtering: true,
      ordering: true
    })
  },
})

module.exports = {
  UnitQuery,
}
