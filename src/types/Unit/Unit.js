const { objectType } = require('nexus')

const Unit = objectType({
  name: 'Unit',
  definition(t) {
    t.model.id()
    t.model.name()
    t.model.staff()
    t.model.department()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Unit,
}
