const { objectType } = require('nexus')

const Department = objectType({
  name: 'Department',
  definition(t) {
    t.model.id()
    t.model.name()
    t.model.units()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Department,
}
