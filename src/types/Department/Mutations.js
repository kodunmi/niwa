const { stringArg, nonNull, extendType, intArg } = require("nexus")

const DepartmentMutation = extendType({
  type: 'Mutation',
  definition(t) {
    t.field('createDepartment',{
      type: 'Department',
      args: {
        name: nonNull(stringArg())
      },
      resolve: async (p, {name}, ctx) => {
        return await ctx.prisma.department.create({
          data: {
            name
          }
        })
      }
    })
    t.field('createDepartment',{
      type: 'Department',
      args: {
        name: nonNull(stringArg())
      },
      resolve: async (p, {name}, ctx) => {
        return await ctx.prisma.department.create({
          data: {
            name
          }
        })
      }
    })
    t.field('editDepartment',{
      type: 'Department',
      args: {
        id: nonNull(intArg()),
        name: nonNull(stringArg())
      },
      resolve: async (p, {id, name}, ctx) => {
        return await ctx.prisma.department.update({
          where:{
            id: id
          },
          data: {
            name
          }
        })
      }
    })

    t.field('deleteDepartment',{
      type: 'Department',
      args: {
        id: nonNull(intArg()),
      },
      resolve: async (p, {id, name}, ctx) => {
        return await ctx.prisma.department.delete({
          where:{
            id: id
          }
        })
      }
    })
  }
})
module.exports = {
  DepartmentMutation,
}