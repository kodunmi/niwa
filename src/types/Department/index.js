module.exports = {
    ...require('./Department'),
    ...require('./Mutations'),
    ...require('./Query'),
}