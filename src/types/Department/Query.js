const { queryType, extendType } = require('nexus')

const DepartmentQuery = extendType({
  type: 'Query',
  definition(t) {
   t.crud.departments({
     filtering: true,
     ordering: true
   })
  },
})

module.exports = {
  DepartmentQuery,
}
