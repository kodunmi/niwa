const { nullable, stringArg, nonNull, extendType, intArg, arg } = require("nexus")

const EventMutation = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createEvent', {
            type: 'Event',
            args: {
                name: nonNull(stringArg()),
                about: nullable(stringArg()),
                date: nullable(stringArg()),
                venue: nullable(stringArg()),
            },
            resolve: async (p, { about, date, venue, name}, ctx) => {

                return await ctx.prisma.event.create({
                    data: {
                        about,
                        date,
                        venue,
                        name
                    }
                })
            }
        })

        t.field('updateEvent', {
            type: 'Event',
            args: {
                id: nonNull(intArg()),
                name: nullable(stringArg()),
                about: nullable(stringArg()),
                date: nullable(stringArg()),
                venue: nullable(stringArg()),
            },
            resolve: async (p, {id, name, about, date, venue}, ctx) => {
                return await ctx.prisma.event.update({
                    where:{
                        id
                    },

                    data:{
                        name,
                        about,
                        date,
                        venue
                    }
                })
            }
        })

        t.field('deleteEvent', {
            type: 'Event',
            args: {
                id : nonNull(intArg())
            },
            resolve: async (p, { id }, ctx) => {
                return await ctx.prisma.event.delete({
                    where: {
                        id
                    }
                })
            }
        })

    }
})

module.exports = {
    EventMutation
}