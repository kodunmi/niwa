const { queryType, extendType } = require('nexus')

const EventQuery = extendType({
  type:'Query',
  definition(t) {
   t.crud.events({
     ordering: true,
     filtering: true
   })
  },
})

module.exports = {
  EventQuery,
}
