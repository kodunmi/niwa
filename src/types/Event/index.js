module.exports ={
    ...require('./Mutation'),
    ...require('./Event'),
    ...require('./Query')
}