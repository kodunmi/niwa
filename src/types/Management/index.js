module.exports = {
    ...require('./Mutations'),
    ...require('./Query'),
    ...require('./Worker'),
    ...require('./Position')
}