const { objectType } = require('nexus')

const Worker = objectType({
  name: 'Worker',
  definition(t) {
    t.model.id()
    t.model.firstname()
    t.model.lastname()
    t.model.unit()
    t.model.position()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Worker,
}
