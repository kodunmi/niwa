const { objectType } = require('nexus')

const Position = objectType({
  name: 'Position',
  definition(t) {
    t.model.id()
    t.model.name()
    t.model.staff()
  },
})

module.exports = {
  Position,
}
