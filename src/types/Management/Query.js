const {  queryType, extendType } = require('nexus')

const ManagementQuery = extendType({
  type:'Query',
  definition(t) {
   t.crud.workers({
     filtering: true,
     ordering: true
   })
   t.crud.positions({
     filtering: true,
     ordering: true
   })
  },
})

module.exports = {
  ManagementQuery,
}
