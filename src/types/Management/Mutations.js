const { nullable, nonNull, stringArg, intArg, extendType } = require("nexus")

const WorkerMutation = extendType({
  type:'Mutation',
  definition(t) {
    t.field('createWorker',{
      type: 'Worker',
      args: {
        firstname: nonNull(stringArg()),
        lastname: nonNull(stringArg()),
        unit: nonNull(stringArg()),
        position: nonNull(intArg())
      },
      resolve: async (p, {firstname, lastname, unit, position}, ctx) => {
        return await ctx.prisma.worker.create({
          data: {
            firstname,
            lastname,
            unit:{
              connect:{
                id: unit
              }
            },
            position:{
              connect:{
                id: position
              }
            }

          }
        })
      }
    })
    t.field('editWorker',{
      type: 'Worker',
      args: {
        id: nonNull(intArg()),
        firstname: nullable(stringArg()),
        lastname: nullable(stringArg()),
        unit: nullable(stringArg()),
        position: nullable(intArg())
      },
      resolve: async (p, {id, firstname, lastname, unit, position}, ctx) => {
        return await ctx.prisma.worker.update({
          where: {
            id
          },
          data: {
            firstname,
            lastname,
            unit:{
              connect:{
                id: unit
              }
            },
            position:{
              connect:{
                id: position
              }
            }

          }
        })
      }
    })
    t.field('deleteWorker',{
      type: 'Worker',
      args: {
        id: nonNull(intArg()),
      },
      resolve: async (p, {id}, ctx) => {
        return await ctx.prisma.worker.delete({
          where: {
            id
          }
        })
      }
    })

    t.field('createPosition',{
      type: 'Position',
      args: {
        name: nonNull(stringArg())
      },
      resolve: async (p, {name}, ctx) => {
        return await ctx.prisma.position.create({
          data:{
            name
          }
        })
      }
    })
    t.field('editPosition',{
      type: 'Position',
      args: {
        id: nonNull(intArg()),
        name: nullable(stringArg())
      },
      resolve: async (p, {id, name}, ctx) => {
        return await ctx.prisma.position.update({
          where:{
            id
          },
          data:{
            name
          }
        })
      }
    })
    t.field('deletePosition',{
      type: 'Position',
      args: {
        id: nonNull(intArg())
      },
      resolve: async (p, {id, name}, ctx) => {
        return await ctx.prisma.position.delete({
          where:{
            id
          }
        })
      }
    })
  }
})
module.exports = {
  WorkerMutation,
}