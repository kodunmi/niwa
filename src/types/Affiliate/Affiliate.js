const { objectType } = require('nexus')

const Affiliate = objectType({
  name: 'Affiliate',
  definition(t) {
    t.model.id()
    t.model.name()
    t.model.public_id()
    t.model.secure_url()
  },
})

module.exports = {
  Affiliate,
}
