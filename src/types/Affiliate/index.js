module.exports ={
    ...require('./Mutation'),
    ...require('./Affiliate'),
    ...require('./Query')
}