const { queryType, extendType } = require('nexus')

const AffiliateQuery = extendType({
  type:'Query',
  definition(t) {
   t.crud.affiliates({
     ordering: true,
     filtering: true
   })
  },
})

module.exports = {
  AffiliateQuery,
}
