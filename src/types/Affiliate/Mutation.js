const { nullable, stringArg, nonNull, extendType, intArg, arg } = require("nexus")

const AffiliateMutation = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createAffiliate', {
            type: 'Affiliate',
            args: {
                name: nonNull(stringArg()),
                image: nonNull(stringArg()),
            },
            resolve: async (p, { name, image }, ctx) => {

                const { secure_url, public_id } = image ? await uploadFile(image) : {}

                return await ctx.prisma.affiliate.create({
                    data: {
                        name,
                        secure_url,
                        public_id
                    }
                })
            }
        })

        t.field('deleteAffiliate', {
            type: 'Affiliate',
            args: {
                id : nonNull(intArg())
            },
            resolve: async (p, { id }, ctx) => {
                return await ctx.prisma.affiliate.delete({
                    where: {
                        id
                    }
                })
            }
        })

    }
})

module.exports = {
    AffiliateMutation
}