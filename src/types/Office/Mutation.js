const { nullable, stringArg, nonNull, extendType, intArg, arg } = require("nexus")

const OfficeMutation = extendType({
    type: 'Mutation',
    definition(t) {
        t.field('createOffice', {
            type: 'Office',
            args: {
                location: nonNull(stringArg()),
                address: nonNull(stringArg()),
            },
            resolve: async (p, { location, address }, ctx) => {

                return await ctx.prisma.office.create({
                    data: {
                        location,
                        address
                    }
                })
            }
        })

        t.field('updateOffice', {
            type: 'Office',
            args: {
                id: nonNull(intArg()),
                location: nullable(stringArg()),
                address: nullable(stringArg()),
            },
            resolve: async (p, { location, address }, ctx) => {
                return await ctx.prisma.office.update({
                    where: {
                        id
                    },

                    data: {
                        location,
                        address
                    }
                })
            }
        })

        t.field('deleteOffice', {
            type: 'Office',
            args: {
                id: nonNull(intArg())
            },
            resolve: async (p, { id }, ctx) => {
                return await ctx.prisma.office.delete({
                    where: {
                        id
                    }
                })
            }
        })

        t.field('makeHeadOffice', {
            type: 'Office',
            args: {
                id: nonNull(intArg())
            },
            resolve: async (p, { id }, ctx) => {

                const currentHeadOffice = await ctx.prisma.office.findFirst({
                    where: {
                        isHeadOffice: true
                    }
                })

                if (currentHeadOffice) {
                    await ctx.prisma.office.update({
                        where: {
                            id: currentHeadOffice.id
                        },

                        data: {
                            isHeadOffice: false
                        }
                    })
                }

                return await ctx.prisma.office.update({
                    where: {
                        id
                    },

                    data: {
                        isHeadOffice: true
                    }
                })
            }
        })

    }
})

module.exports = {
    OfficeMutation
}