const { objectType } = require('nexus')

const Event = objectType({
  name: 'Event',
  definition(t) {
    t.model.id()
    t.model.name()
    t.model.about()
    t.model.date()
    t.model.venue()
    t.model.createdAt()
    t.model.updatedAt() 
  },
})

module.exports = {
  Event,
}
