module.exports ={
    ...require('./Mutation'),
    ...require('./Office'),
    ...require('./Query')
}