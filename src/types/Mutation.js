// const { compare, hash } = require('bcryptjs')
// const { sign } = require('jsonwebtoken')
// const { nullable, mutationType, stringArg, arg , intArg, nonNull} = require('nexus')
// const { APP_SECRET, uploadFile, Upload, deleteFile, updateFile } = require('../utils')
// const sendMail = require("../helpers/mail/index");
// const cryptoRandomString = require('crypto-random-string');

// const Mutation = mutationType({
//   definition(t) {
//     t.field('signup', {
//       type: 'AuthPayload',
//       args: {
//         firstname: nonNull(stringArg()),
//         lastname: nonNull(stringArg()),
//         email: nonNull(stringArg()),
//         password: nonNull( stringArg()),
//         phone: nullable(stringArg()),
//         image: nullable(arg({type: Upload})),
//       },
//       resolve: async (parent, {image, firstname, lastname, email, password, phone }, ctx) => {
//         const hashedPassword = await hash(password, 10)
//         const generatedCode = cryptoRandomString({ length: 6, type: 'numeric' })

//         const usercheck = await ctx.prisma.user.findUnique({
//           where: {
//             email: email
//           }
//         })

//         if(usercheck) return new Error("email already taken") 

//         const { secure_url, public_id } = image ? await uploadFile(image) : {}

//         const user = await ctx.prisma.user.create({
//           data: {
//             email,
//             password: hashedPassword,
//             code: Number(generatedCode),
//             profile: {
//               create: {
//                 phone,
//                 firstname,
//                 lastname,
//                 image: image ? secure_url : undefined,
//                 public_id: image ?  public_id : undefined,
//               }
//             }
//           },
//         })

//         //send verification mail
//         let response = {
//           body: {
//             name: firstname,
//             intro: 'Your email verification code is: ' + generatedCode + '',
//             action: {
//               instructions: 'to continue, please click here:',
//               button: {
//                 color: '#22BC66', // Optional action button color
//                 text: 'Confirm your account',
//                 link: 'https://mailgen.js/confirm?s=d9729feb74992cc3482b350163a1a010'
//               },
//               outro: 'If you did not create an account, no further action is required. /n Thanks'
//             },
//           }
//         }

//         sendMail.sendMail(response, user.email, "Verify User");

//         return {
//           token: sign({ userId: user.id }, APP_SECRET),
//           user,
//         }
//       },
//     })

//     t.field('login', {
//       type: 'AuthPayload',
//       args: {
//         email: nonNull(stringArg()) ,
//         password: nonNull(stringArg()),
//       },
//       resolve: async (parent, { email, password }, context) => {
//         const user = await context.prisma.user.findUnique({
//           where: {
//             email,
//           },
//         })
//         if (!user) {
//           throw new Error(`No user found for email: ${email}`)
//         }
//         const passwordValid = await compare(password, user.password)
//         if (!passwordValid) {
//           throw new Error('Invalid password')
//         }
//         return {
//           token: sign({ userId: user.id }, APP_SECRET),
//           user,
//         }
//       },
//     })

//     //resend verification mail

//     t.field('resendVerificaitonEmail', {
//       type: 'AuthPayload',
//       args: {
//         id: nonNull(intArg())
//       },
//       resolve: async (parent, { id }, context) => {
//         const user = await context.prisma.user.findUnique({
//           where: {
//             id: id
//           },
//           select: {
//             profile: {

//             }
//           }

//         })

//         if (!user) return new Error('no user found for')

//         if (user.verification === 'VERIFIED') return new Error('user already verified')

//         let response = {
//           body: {
//             name: user.profile.firstname,
//             intro: 'Your email verification code is: ' + user.code + '',
//             action: {
//               instructions: 'to continue, please click here:',
//               button: {
//                 color: '#22BC66', // Optional action button color
//                 text: 'Confirm your account',
//                 link: 'https://mailgen.js/confirm?s=d9729feb74992cc3482b350163a1a010'
//               },
//               outro: 'If you did not create an account, no further action is required. /n Thanks'
//             },
//           }
//         }

//         sendMail.sendMail(response, user.email, "Verify User");

//         return {
//           token: sign({ userId: user.id }, APP_SECRET),
//           user,
//         }
//       }
//     })
//     //email verification

//     t.field('verifyEmail', {
//       type: 'AuthPayload',
//       args: {
//         id: nonNull(intArg()),
//         code: nonNull(intArg()),
//       },

//       resolve: async (parent, { id, code }, context) => {
//         const checkuser = await context.prisma.user.findUnique({
//           where: {
//             id: id
//           }
//         })

//         if (!checkuser) {
//           return new Error('User not found')

//         } else {
//           const checkcode = checkuser.code === code

//           if (!checkcode) return new Error('invalid code')

//           const user = await context.prisma.user.update({
//             where: {
//               id: id
//             },
//             data: {
//               verifiedAt: new Date().toISOString()
//             }
//           })

//           return {
//             token: sign({ userId: user.id }, APP_SECRET),
//             user,
//           }
//         }
//       }
//     })

//     t.field('sendPasswordResetMail', {
//       type: 'User',
//       args: {
//         email: nonNull(stringArg())
//       },
//       resolve: async (p, { email }, context) => {
//         const user = await context.prisma.user.findUnique({
//           where: {
//             email: email
//           },
//           select: {
//             profile: {

//             }
//           }
//         })

//         if (!user) return new Error('no user with such email')

//         let response = {
//           body: {
//             name: user.profile.firstname,
//             intro: `you requested for a password reset`,
//             action: {
//               instructions: 'to reset password, please click here:',
//               button: {
//                 color: '#22BC66', // Optional action button color
//                 text: 'reset password',
//                 link: `https://biki.com/password-reset?c=${user.password}?d=${Date.now}`
//               },
//               outro: 'If you did not request for password reset, no further action is required. /n Thanks'
//             },
//           }
//         }

//         sendMail.sendMail(response, user.email, "reset password");

//         return user

//       }
//     })

//     t.field('resetPassword', {
//       type: 'User',
//       args: {
//         id: nonNull(intArg()),
//         password: nonNull(stringArg({description: "the new password to be stored in the database" }))
//       },

//       resolve: async (p, { id, password }, context) => {

//         const checkuser = await context.prisma.user.findUnique({
//           where: {
//             id: id
//           },

//           select: {
//             profile: {

//             }
//           }

//         })

//         if (!checkuser) return new Error('no such user is found')

//         const hashedpassword = await hash(password, 10)
//         const user = await context.prisma.user.update({
//           where: {
//             id: id
//           },
//           data: {
//             password: hashedpassword
//           }
//         })

//         let response = {
//           body: {
//             name: checkuser.profile.firstname,
//             intro: `your password has been reset`,
//             action: {
//               instructions: 'to login, please click here:',
//               button: {
//                 color: '#22BC66', // Optional action button color
//                 text: 'login to dashboard',
//                 link: `https://biki.com/login`
//               },
//               outro: 'If you did not change your password, please contact admin@biki.com for clearfication. /n Thanks'
//             },
//           }
//         }

//         sendMail.sendMail(response, user.email, "password updated successfully");

//         return user
//       }
//     })

//     t.field('profileUpdate', {
//       type: 'User',
//       args: {
//         id: nonNull(intArg()),
//         firstname: stringArg(),
//         lastname: stringArg(),
//         phone: stringArg(),
//         email: stringArg(),
//         password: stringArg(),
//         image: arg({
//           type: Upload,
//         }),
//       },
//       resolve: async (parent, { id, firstname, lastname, phone, image, email, password }, ctx) => {
//         const hashedPassword = password ? await hash(password, 10) : undefined

//         const usercheck = await ctx.prisma.user.findUnique({
//           where:{
//             id:id
//           },
//           select:{
//             profile:{
//               select:{
//                 public_id:true
//               }
//             }
//           }
//         })

//         if(!usercheck) return new Error("no user found with the id")

//         if(image) file = await updateFile(image, usercheck.profile.public_id)

//         return await ctx.prisma.user.update({
//           where: {
//             id: id
//           },
//           data: {
//             email,
//             password: hashedPassword,
//             profile: {
//               update: {
//                 firstname,
//                 lastname,
//                 phone,
//                 secure_url: file.secure_url,
//                 public_id: file.public_id
//               }
//             }
//           },
//         })
//       },
//     })
//   },
// })

// module.exports = {
//   Mutation,
// }
