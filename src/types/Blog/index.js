module.exports = {
    ...require('./Mutations'),
    ...require('./Post'),
    ...require('./Comment'),
    ...require('./Tag'),
    ...require('./Query'),
}