const { objectType } = require('nexus')

const Tag = objectType({
  name: 'Tag',
  definition(t) {
    t.model.id()
    t.model.posts()
    t.model.name()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Tag,
}
