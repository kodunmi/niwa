const { objectType } = require('nexus')

const Post = objectType({
  name: 'Post',
  definition(t) {
    t.model.id()
    t.model.title()
    t.model.body()
    t.model.secure_url()
    t.model.public_id()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Post,
}
