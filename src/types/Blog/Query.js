const { queryType, extendType } = require('nexus')

const BlogQuery = extendType({
  type:'Query',
  definition(t) {
   t.crud.posts({
     ordering: true,
     filtering: true
   })
  },
})

module.exports = {
  BlogQuery,
}
