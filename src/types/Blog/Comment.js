const { objectType } = require('nexus')

const Comment = objectType({
  name: 'Comment',
  definition(t) {
    t.model.id()
    t.model.post()
    t.model.name()
    t.model.email()
    t.model.body()
    t.model.replies()
    t.model.createdAt()
    t.model.updatedAt()
  },
})

module.exports = {
  Comment,
}
