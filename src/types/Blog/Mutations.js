const { nullable, stringArg, nonNull, extendType, intArg, arg, list } = require("nexus")
const { Upload } = require("../../utils")

const BlogMutation = extendType({
    type: 'Mutation',
    definition(t) {
        // t.field('createCategory', {
        //     type: 'Category',
        //     args: {
        //         name: nonNull(stringArg()),
        //     },

        //     resolve: async (p, { name }, ctx) => {
        //         return await ctx.prisma.category.create({
        //             data: {
        //                 name
        //             }
        //         })
        //     }
        // }

        // )

        // t.field('editCategory', {
        //     type: 'Category',
        //     args: {
        //         id: nonNull(intArg()),
        //         name: nullable(stringArg()),
        //     },

        //     resolve: async (p, { name, id}, ctx) => {
        //         return await ctx.prisma.category.update({
        //             where:{
        //                 id: id
        //             },
        //             data: {
        //                 name
        //             }
        //         })
        //     }
        // }

        // )

        // t.field('deleteCategory', {
        //     type: 'Category',
        //     args: {
        //         id: nonNull(intArg())
        //     },

        //     resolve: async (p, {id}, ctx) => {
        //         return await ctx.prisma.category.delete({
        //             where:{
        //                 id: id
        //             }
        //         })
        //     }
        // }

        // )


        t.field('createPost', {
            type: 'Post',
            args: {
                author: nonNull(intArg()),
                title: nonNull(stringArg()),
                body: nonNull(stringArg()),
                image: nullable(arg({ type: Upload })),
                tags: list(intArg())
            },

            resolve: async (p, { author, title, body, image , tags}, ctx) => {

                const { secure_url, public_id } = image ? await uploadFile(image) : {}

                return await ctx.prisma.post.create({
                    data: {
                        secure_url,
                        public_id,
                        title,
                        body,

                        author: {
                            connect: {
                                id: author
                            }
                        }
                    }
                })
            }
        }

        )

        t.field('editPost', {
            type: 'Post',
            args: {
                id: nonNull(intArg()),
                author: nullable(intArg()),
                title: nullable(stringArg()),
                body: nullable(stringArg()),
                image: nullable(arg({ type: Upload })),
            },

            resolve: async (p, { id, author, title, body, image }, ctx) => {

                const post = await ctx.prisma.post.findUnique({
                    where: {
                        id: id
                    }
                })

                if (!post) return new Error(`no post with ${id} is available`)

                if (image) file = await updateFile(image, post.public_id)

                return await ctx.prisma.post.update({
                    where: {
                        id
                    },
                    data: {
                        secure_url: image ? file.secure_url : undefined,
                        public_id: image ? file.public_id : undefined,
                        title,
                        body,

                        author: {
                            connect: {
                                id: author
                            }
                        }
                    }
                })
            }
        }

        )

        t.field('deletePost', {
            type: 'Post',
            arg: {
                id: nonNull(intArg())
            },

            resolve: async (p, { id }, ctx) => {
                const post = await ctx.prisma.post.findUnique({
                    where: {
                        id: id
                    }
                })

                if (!post) return new Error(`no post with ${id} is available`)

                await deleteFile(post.public_id)

                return ctx.prisma.post.delete({
                    where: {
                        id: id
                    },
                })

            }
        })

        t.field('addTag', {
            type: 'Tag',
            args: {
                name: nonNull(stringArg())
            },
            resolve: async (p, { name }, ctx) => {
                return await ctx.prisma.tag.create({
                    data: {
                        name
                    }
                })
            }
        })

        t.field('deleteTag', {
            type: 'Tag',
            args: {
                id: nonNull(intArg())
            },
            resolve: async (p, { id }, ctx) => {
                return await ctx.prisma.tag.delete({
                    where: {
                        id
                    }
                })
            }
        })
    }
})
module.exports = {
    BlogMutation,
}